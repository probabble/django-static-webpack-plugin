# Django Static Webpack Plugin

[![npm version](https://badge.fury.io/js/django-static-webpack-plugin.svg)](https://badge.fury.io/js/django-static-webpack-plugin)

[![NPM](https://nodei.co/npm/django-static-webpack-plugin.png?compact=true)](https://nodei.co/npm/django-static-webpack-plugin/)

Transforms links in the generated [html-webpack-plugin](https://github.com/jantimon/html-webpack-plugin) html output to Django static template tags, ex: `'bundles/js/main.js' ==> {% static 'bundles/js/main.js' %}`

This is an extension plugin for the [webpack](http://webpack.github.io) plugin [html-webpack-plugin](https://github.com/jantimon/html-webpack-plugin) - a plugin that simplifies the creation of HTML files to serve your webpack bundles.

## Installation

Install the plugin with npm:

```shell
npm install django-static-webpack-plugin --save-dev
```

## Basic Usage

Add the plugin to your webpack config as follows:

```javascript
const DjangoStaticWebpackPlugin = require('django-static-webpack-plugin')
```

`webpack.config.js`:

```javascript
module.exports = {
  ...
  plugins: [
    new DjangoStaticWebpackPlugin({
      bundlePath: 'my/bundle/path'
    })
  ]
}
```

Note: this plugin should be added after HtmlWebpackPlugin.

`vue.config.js`:

```javascript
module.exports = {
  ...
  configureWebpack: {
    plugins: [
      new DjangoStaticWebpackPlugin({
        bundlePath: 'my/bundle/path'
      })
    ]
  }
}
```

## Options

The plugin the following options:

### bundlePath

The relative path of the webpack generated bundle to django static directory.

If django static files are in `/www/my_django/my_django/static/` and the webpack generated bundle is in `/www/my_django/my_django/static/bundles/webpack/` then the argument would be: `bundles/webpack`

the transformed links would be similar to:
`{% static 'bundles/webpack/js/main_page.1a8660b3.js' %}`

### excludeFilenames

An array of HtmlWebpackPlugin output filenames to be excluded from processing by this plugin, ex: ['index.html']
